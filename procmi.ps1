

foreach($proc in $args){
  $fileName = (($proc).ToString() + '--' + (Get-Date -uformat "%Y%m%d-%H%M%S") + ".meminfo")
  Write-Output ("******** Minne info om prosess med PID " + $proc + " ********") >> $fileName
  Write-Output ("Total bruk av virtuelt minne: " + (Get-Process | Where-Object {$_.Id -eq $proc}).VirtualMemorySize/1048576 + "MB") >> $fileName
  Write-Output ("St�rrelse p� Working Set: " + ((Get-Process | Where-Object {$_.Id -eq $proc})).WorkingSet/10024 + "KB") >> $fileName
}
