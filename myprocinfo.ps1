
## Funksjoner
function meny {
  Write-Output "  1 - Hvem er jeg og hva er navnet p˚a dette scriptet?
  2 - Hvor lenge er det siden siste boot?
  3 - Hvor mange prosesser og tr˚ader finnes?
  4 - Hvor mange context switch'er fant sted siste sekund?
  5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund?
  6 - Hvor mange interrupts fant sted siste sekund?
  9 - Avslutt dette scriptet
  "
}

function last_bootUp {
  $tid = ((Get-CimInstance Win32_OperatingSystem).LocalDateTime - (Get-CimInstance Win32_OperatingSystem
  ).LastBootUpTime)
  $timer = ($tid).Hours
  $min = ($tid).Minutes
  $sek = ($tid).Seconds

  Write-Output "Tid siden siste bootup(t:m:s):" $timer : $min : $sek
}

#for virtuell minne osv: Get-Process | Get-Member | more
function prosesser_traader {
  Get-Process * | ForEach-Object {$tot += $_.Threads.count}

  Write-Output ("Antall tråder: " + $tot)
  Write-Output ("Antall prosesser: " + (Get-Process).Count)
}

function context_switch {
  $ct = ((Get-Counter -Counter "\System\Kontekstvekslinger/sek").CounterSamples).CookedValue
  Write-Output "Antall context-switcher siste sek:" $ct
}

function cpu_tid {
  $brukermod = ((Get-Counter '\Prosessor(_Total)\% Brukermodustid').CounterSamples).CookedValue
  $kernel = ((Get-Counter '\Prosessor(_Total)\% systemmodustid').CounterSamples).CookedValue

  Write-Output "CPU- tid, Brukermodus: " $brukermod% "Kernel: " $kernel%
}

function antall_intr {
  $intr = ((Get-Counter '\Prosessor(_total)\Avbrudd/sek').CounterSamples).CookedValue
  Write-Output "Antall interrupts siste sek:" $intr
}


## Main
$ans=$TRUE
while ($ans) {
  meny
  Write-Output "Skriv inn nr:"
  $ans = Read-Host
  switch ( $ans ) {
    1 { Write-Output "I am"$env:username "and this script's name is" $MyInvocation.InvocationName;  break}
    2 { last_bootUp; break}
    3 { prosesser_traader; break}
    4 { context_switch; break}
    5 { cpu_tid; break}
    6 { antall_intr; break}
    9 {Write-Output "Avslutter..."; $ans=$FALSE; break}
    default { Write-Output "$ans er ugyldig valg "; break;}
  }
}
